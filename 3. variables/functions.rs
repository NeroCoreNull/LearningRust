
// Rust code uses snake case as the conventional style for function and variable names, in which all letters are lowercase and underscores separate words.

//We can define functions to have parameters, which are special variables that are part of a function’s signature. When a function has parameters, you can provide it with concrete values for those parameters. Technically, the concrete values are called arguments, but in casual conversation, people tend to use the words parameter and argument interchangeably for either the variables in a function’s definition or the concrete values passed in when you call a function.
pub fn subfunc(){
    another_subfunc(5);
    third_subfunc(5, 'h');
    
    subfunc_returns();
}

fn another_subfunc(x: i32){
    println!("The value of x is: {x}");
}

fn third_subfunc(value: i32, unit_label: char){
    println!("The measurement is: {value}{unit_label}");
}

// Function bodies are made up of a series of statements optionally ending in an expression.
    // Statements are instructions that perform some action and do not return a value.
    // Expressions evaluate to a resultant value. Let’s look at some examples.
    
//Calling a function is an expression. Calling a macro is an expression. A new scope block created with curly brackets is an expression, for example
fn fourth_subfunc(){
    let y = {
        let x = 3;
        x + 1
    };
    println!("{y}");
}

// Functions can return values to the code that calls them. We don’t name return values, but we must declare their type after an arrow (->). In Rust, the return value of the function is synonymous with the value of the final expression in the block of the body of a function. You can return early from a function by using the return keyword and specifying a value, but most functions return the last expression implicitly. Here’s an example of a function that returns a value:
fn five() -> i32 {
    5
}

fn plus_one(one: i32) -> i32 {
    one + 1
}

fn subfunc_returns(){
    let x = five();
    let y = plus_one(x);
    
    println!("The value of x is: {y}.");
}