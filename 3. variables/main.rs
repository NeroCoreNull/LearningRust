#![allow(unused)]

mod compound_types;
mod control_flow;
mod functions;

use std::io;

fn main() {
    // let mut x = 5;
    // println!("The value of x is {x}");
    // x = 6;
    // println!("The value of x is {x}");

    // shadow();
    // data_types();

    //compound_types::sub_class();

    //functions::subfunc();

    control_flow::main();
}

fn shadow() {
    // set var x
    let x = 5;
    // shadow var x
    let x: i32 = x + 1;
    // opening an inner scope
    {
        // shadowing var x inside of inner scope
        let x = x * 2;
        println!("The value of x in the inner scope is {x}");
        // Inner scope ended, var set to last outer scope value
    }
    // Prints last same scope value
    // The inner scope ended so it defaults back to the last declared
    println!("The value of x in the outer scope is {x}");

    // more ways to shadow
    // declare var type string
    let spaces = "    ";
    // shadow var and change its type from String
    let spaces = spaces.len();
    println!("The length (len()) is: {} spaces long", spaces);
}

fn data_types() {
    // ask usr for input
    print!("Please enter a number: ");
    // usr_input is a mutable string
    let mut usr_input = String::new();

    // read the line, expect a mutable string and use .expect for errors
    io::stdin()
        .read_line(&mut usr_input)
        .expect("Err to read line");
    // The input returns it to the string: usr_input.
    // what is returned is 'Result<usr_input>'

    // assign guess as an unassigned 32 bit int
    let guess: u32 = usr_input.trim().parse().expect("Err");
    // trim off the 'Result<>' and parse it to be a u32 int. use .expect for errors
    println!("{}", guess);

    // Call the 4 types of scaler types:
    integer_nums();
    floating_point_nums();
}

// A scalar type represents a single value.
//Rust has four primary scalar types:

// 1. integers:
fn integer_nums() {
    // This is a signed num
    // i = can possibly be a negative number (signed, like a negative *sign*) || explicitely *signed* positive or neg
    let signed_num: i32 = 24;
    println!("{}", signed_num);

    // This is a unsigned num
    // u = is not and will not be negative || we can assume its positive
    let unsigned_num: u32 = 42;
    println!("{}", unsigned_num);

    // isize and usize are either 32 bit or 64 bit. it depends on the computer it is running on
    //So how do you know which type of integer to use? If you’re unsure, Rust’s defaults are generally good places to start: integer types default to i32. The primary situation in which you’d use isize or usize is when indexing some sort of collection.
    let auto_determine_bit: usize = 210;
    println!("{}", auto_determine_bit);

    // Number literals	         Example
    //     Decimal	             98_222
    //       Hex	              0xff
    //      Octal	              0o77
    //      Binary	           0b1111_0000
    //   Byte (u8 only)	           b'A'
    let types_of_nums: [u32; 4] = [98_222, 0xff, 0o77, 0b1111_0000];
    let cant_be_u32_because_its_stupid: u8 = b'A';
    print!(
        "Decimal: {0}\nHex: {1}\nOctal: {2}\nBinary: {3}\nByte: {4}",
        types_of_nums[0],
        types_of_nums[1],
        types_of_nums[2],
        types_of_nums[3],
        cant_be_u32_because_its_stupid
    );
}

// 2. floating point numbers (floats)
fn floating_point_nums() {
    // rust has 2 floats: f32 and f64 (only use f64 unless odd use case because f64 is used by default)
    let x: f64 = 2.0; // f64
    let y: f32 = 3.0; // f32

    // addition
    let sum = 5 + 10;

    // subtraction
    let difference = 95.5 - 4.3;

    // multiplication
    let product = 4 * 30;

    // division
    let quotient = 56.7 / 32.2;
    let truncated = -5 / 3; // Results in -1

    // remainder
    let remainder = 43 % 5;
}

// Booleans
fn boolean_values() {
    // The main way to use Boolean values is through conditionals, such as an if expression.
    let t = true; // editor auto-added because its implied
    let f: bool = false; // explicitely stated
}

// characters
fn character_types() {
    let c = 'z'; // implied char type
    let z: char = 'ℤ'; // with explicit type annotation
    let heart_eyed_cat = '😻'; // implied char type

    //Note that we specify char literals with single quotes, as opposed to string literals, which use double quotes. Rust’s char type is four bytes in size and represents a Unicode Scalar Value, which means it can represent a lot more than just ASCII. Accented letters; Chinese, Japanese, and Korean characters; emoji; and zero-width spaces are all valid char values in Rust. Unicode Scalar Values range from U+0000 to U+D7FF and U+E000 to U+10FFFF inclusive.
}
