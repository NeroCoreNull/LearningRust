use std::io;

// The ability to run some code depending on whether a condition is true and to run some code repeatedly while a condition is true are basic building blocks in most programming languages. The most common constructs that let you control the flow of execution of Rust code are if expressions and loops.

pub fn main() {
    println!("Enter a number greater than or less than 5: ");
    let mut number = String::new();
    io::stdin().read_line(&mut number).expect("Err");
    let number = number.trim().parse().expect("Err");

    println!("The number you chose is less than 5: {}.", branches(number));
}

fn branches(number: i32) -> bool {
    if number < 5 {
        true
    } else {
        false
    }
}
