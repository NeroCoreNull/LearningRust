use std::io::{self, stdin};

pub fn sub_class(){
    tuple_types();
    array_examples();
    array_call();
}

fn tuple_types(){
    // explicit typing
    let tup: (i32, f64, u8) = (500, 6.4, 1);
    println!("{}", tup.0);
    
    // implied. The editor is inferring these types
    let tup2 = (500, 3.14, 1);
    let (x, y, z) = tup;
    println!("{}", y);
    
    // add inner scope for var shadowing
    {
        let x: (i32, f64, u8)= (500, 3.14, 1);
        let five_hundred = x.0;
        let pi = x.1;
        let one = x.2;
    }
    
    // A tuple without any values is called a unit.
    let unit: () = ();
    //represent an empty value or an empty return type. Expressions implicitly return the unit value if they don’t return any other value.
}

fn array_examples(){
    //Another way to have a collection of multiple values is with an array. Unlike a tuple, every element of an array must have the same type. Unlike arrays in some other languages, arrays in Rust have a fixed length.
    let a = [1, 2, 3, 4, 5];
    
    let months = ["January", "February", "March", "April", "May", "June", "July",
                  "August", "September", "October", "November", "December"];
    
    // implicit (still has the array length)
    let a = [1, 2, 3, 4, 5];
    // explicit 
    let a: [i32; 5] = [1, 2, 3, 4, 5];
    
    //The array named a will contain 5 elements that will all be set to the value 3 initially. This is the same as writing let a = [3, 3, 3, 3, 3]; but in a more concise way.
    let a = [3; 5];
    
    //An array is a single chunk of memory of a known, fixed size that can be allocated on the stack. You can access elements of an array using indexing, like this:
    let a = [1, 2, 3, 4, 5];
    let first = a[0];
    let second = a[1];
}

fn array_call(){
    let a = [1, 2, 3, 4, 5];
    
    println!("Please input a number between 0 and 4: ");
    
    let mut index = String::new();
    
    io::stdin().read_line(&mut index).expect("Err");
    let index: usize = index.trim().parse().expect("err");
    
    let usr_choice = a[index];
    println!("The value at index {} is: {}", index, usr_choice);
}